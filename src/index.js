// import 'core-js/es6/map';
// import 'core-js/es6/set'
// import 'raf/polyfill';
// import "babel-polyfill";
import React from 'react';
import { render} from 'react-dom';
import './styles.less';
import App from './components/app';

// const MyComponent = () => (
//     <App/>
// );
//
// export default App;

export default class BarChart {

    init(url, container_id) {
        render(<App url={url}/>, document.getElementById(container_id));
    }

    update(url) {
        window.updateBarChart(url);
    }

}

// window.initBarChart = function(url, container_id) {
//     render(<App url={url}/>, document.getElementById(container_id));
// }
