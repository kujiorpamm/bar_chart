import React, {Component} from 'react';

export default class Bar extends Component {
    constructor(props) {
        super(props);

    }

    render() {

        let _classname = "bar-container " + this.props.position;
        let _style_bc = { height: this.props.data.percent };

        return(
            <div className={_classname}>
                <div className="bar">
                    <div className="label label-name"> {this.props.data.label_name} </div>
                    <div className="bar-content" style={_style_bc}>
                        <div className="label label-actual">
                            <p>{this.props.data.label_executed}</p>
                            <p>{this.props.data.executed}</p>
                        </div>
                    </div>
                    <div className="purse"></div>
                    <div className="label label-percent">{this.props.data.percent}</div>
                    <div className="label label-total">
                        <p>{this.props.data.label_pointed}</p>
                        <p>{this.props.data.pointed}</p>
                    </div>
                </div>
            </div>
        )
    }

}
