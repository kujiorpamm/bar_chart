import React, {Component} from 'react';
import Bar from './bar';

export default class App extends Component{
    constructor(props) {
        super(props);
        this.state = {
            url: this.props.url,
            items: [
                {
                    label_name: 'Доходы',
                    label_pointed: 'Назначено',
                    label_executed: 'Исполнено',
                    percent: '0%',
                    pointed: 0,
                    executed: 0
                },
                {
                    label_name: 'Расходы',
                    label_pointed: 'Назначено',
                    label_executed: 'Исполнено',
                    percent: '0%',
                    pointed: 0,
                    executed: 0
                },
                {
                    label_def: 'Дефицит',
                    label_prof: 'Профицит',
                    label_pointed: 'Назначено',
                    label_executed: 'Исполнено',
                    def: '0',
                    prof: '0'
                }
            ]
        }

        this.updateUrl = this.updateUrl.bind(this);
    }

    componentDidMount() {
        console.log(this.state.url);
        fetch(this.state.url)
            .then(res => res.json())
            .then(
            (result) => {
                console.log(result);
              this.setState({
                isLoaded: true,
                items: result
              });
            },
            (error) => {
                console.log(error);
              this.setState({
                isLoaded: true,
                error
              });
            }
        )

        window.updateBarChart = this.updateUrl;

    }

    updateUrl(url) {
        this.setState({
            url: url
        });
    }

    render() {
        return (
            <div className="budget-bar">
                <Bar position="left" data={this.state.items[0]}  />
                <Bar position="right" data={this.state.items[1]} />
                <div className="extra">
                    <div className="left">
                        <div className="name">{this.state.items[2].label_def}</div>
                        <div className="label">{this.state.items[2].label_pointed}</div>
                        <div className="value">{this.state.items[2].def}</div>
                    </div>
                    <div className="right">
                        <div className="name">{this.state.items[2].label_prof}</div>
                        <div className="label">{this.state.items[2].label_executed}</div>
                        <div className="value">{this.state.items[2].prof}</div>
                    </div>
                </div>
            </div>
        );
    }
}
